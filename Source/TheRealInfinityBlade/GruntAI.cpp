// Fill out your copyright notice in the Description page of Project Settings.

#include "TheRealInfinityBlade.h"
#include "GruntAI.h"


// Sets default values
AGruntAI::AGruntAI()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AGruntAI::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGruntAI::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void AGruntAI::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

