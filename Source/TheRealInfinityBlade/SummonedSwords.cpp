// Fill out your copyright notice in the Description page of Project Settings.

#include "TheRealInfinityBlade.h"
#include "SummonedSwords.h"
#include "Enemy.h" 


ASummonedSwords::ASummonedSwords() {
	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	bIsWeaponActive = true;
	// InitialLifeSpan=3.0f; //dies after 3 seconds
	BoxCollComp = CreateDefaultSubobject<USphereComponent>(TEXT("SSBoxColl"));
	BoxCollComp->SetSimulatePhysics(false);
	SetActorEnableCollision(true);
	//RootComponent = WeaponMesh;
	MoveComp = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("SSMoveComponent"));
	MoveComp->UpdatedComponent = BoxCollComp;
	MoveComp->InitialSpeed = 2000.0f;
	MoveComp->MaxSpeed = 3000.0f;
	MoveComp->bRotationFollowsVelocity = true;
	//MoveComp->bShouldBounce = true;
	//MoveComp->Bounciness = 0.3f;

	BoxCollComp->MoveIgnoreActors.Add(this);
	//BoxCollComp->MoveIgnoreActors.Add(WeaponOwner);
	BoxCollComp->SetSphereRadius(300.0f);
	RootComponent = BoxCollComp;
	WeaponMesh->AttachTo(RootComponent);
	MoveComp->bAutoActivate = true;
	BoxCollComp->SetSimulatePhysics(true);

}

void ASummonedSwords::OnStartAttack()
{
    AttackAC = PlaySoundFX(AttackStartFX);
    if(AttackAC)
    {
        AttackAC->Play();
    }
}


void ASummonedSwords::BeginPlay()
{
    BoxCollComp->OnComponentBeginOverlap.AddDynamic(this, &ASummonedSwords::OnWeaponOverlapBegin); 
	BoxCollComp->bHiddenInGame = false;
	BoxCollComp->OnComponentHit.AddDynamic(this,&ASummonedSwords::OnHit);
}


void ASummonedSwords::SuperAttack()
{}

void ASummonedSwords::Tick(float deltaTime)
{
	Super::Tick(deltaTime);
	//SetActorLocation(GetActorLocation() + MoveComp->Velocity * deltaTime,true);
	if (IsDetonateActive)
	{
		if (DetTimer <= 1.0f)
		{
			BoxCollComp->SetSphereRadius(250.0* DetTimer);
			DetTimer += deltaTime;
			TArray<AActor*> overlap;
			BoxCollComp->GetOverlappingActors(overlap);
			for (auto temp : overlap)
			{
				ACharacter* enemy = Cast<ACharacter>(temp);
				if (enemy)
				{
					enemy->TakeDamage(2.0f, FDamageEvent(), GetInstigatorController(), this);
					UGameplayStatics::SpawnEmitterAtLocation(this, impact_spark, enemy->GetActorLocation());
				}
			}
		}
		else
		{
			this->Destroy();
			IsDetonateActive = false;
		}
	}
}
UAudioComponent* ASummonedSwords::PlaySoundFX(USoundCue* cue)
{
    UAudioComponent* audio = nullptr;
    if(cue)
    {
        audio = UGameplayStatics::SpawnSoundAttached(cue, RootComponent);
    }
    
    return audio;
}

void ASummonedSwords::InitVelocity(const FVector & ShootDirection)
{
	if (MoveComp)
	{
		// set the projectile's velocity to the desired direction
		MoveComp->Velocity = ShootDirection * MoveComp->InitialSpeed;
		//UGameplayStatics::SpawnEmitterAtLocation(this, P_trail, BoxCollComp->GetComponentLocation());
		result_P = UGameplayStatics::SpawnEmitterAttached(P_trail, WeaponMesh);
		result_P->SetRelativeScale3D(FVector(2.0f,2.0f,2.0f));
	}
}

void ASummonedSwords::OnWeaponOverlapBegin(AActor* other, UPrimitiveComponent* OtherComp,
                                           int32 OtherBodyIndex, bool fromSweep,
                                           const FHitResult& result)
{
    bool NullChecks = other && OtherComp && bIsWeaponActive;
	if (NullChecks)
	{
        
        
        AEnemy* enemy = Cast<AEnemy>(result.GetActor());
			if (enemy)
			{
                int32 index = FMath::RandRange(0, DamagedSFXArray.Num()-1);
                DamagedAC = PlaySoundFX(DamagedSFXArray[index]);
                if(DamagedAC)
                    DamagedAC->Play();
                enemy->TakeDamage(WeaponDamage, FDamageEvent(), GetInstigatorController(), this);
				UGameplayStatics::SpawnEmitterAtLocation(this, impact_spark, enemy->GetActorLocation());
			}
    }
}

void ASummonedSwords::OnWeaponOverlapEnd(AActor* other, UPrimitiveComponent* OtherComp,
                                         int32 OtherBodyIndex, bool fromSweep,
                                         const FHitResult& result)
{}

void ASummonedSwords::OnHit(AActor * OtherActor, UPrimitiveComponent * OtherComp, 
	FVector NormalImpulse, const FHitResult & Hit)
{
	
	if(!PressedE){
		FVector vec = MoveComp->Velocity - 2 * (FVector::DotProduct(MoveComp->Velocity, Hit.ImpactNormal))*Hit.ImpactNormal;
		
		vec.Normalize();
		ASummonedSwords* range = GetWorld()->SpawnActor<ASummonedSwords>(this->GetClass(), this->GetActorLocation(),
			FRotator(0.0f, 0.0f, 0.0f));
		vec.Normalize();
		if (range)
		{
			range->SetWeaponOwner(this->WeaponOwner);
			range->InitVelocity(vec);
		}
		this->Destroy();
		//MoveComp->Velocity = vec;
		//vec.Normalize();
		//this->SetActorRotation(vec.Rotation());
	}
	else { 
			MoveComp->Deactivate();
			UGameplayStatics::SpawnEmitterAtLocation(this, DetonateExplosion, this->GetActorLocation());
			IsDetonateActive = true;
			result_P->DeactivateSystem();
			
	}
}

void ASummonedSwords::Detonate()
{
	MoveComp->Deactivate();
	FVector up = WeaponOwner->GetActorUpVector();
	up *= -1;
	MoveComp->Activate();
	MoveComp->Velocity = up*(MoveComp->InitialSpeed/4);
	PressedE = true;
}

