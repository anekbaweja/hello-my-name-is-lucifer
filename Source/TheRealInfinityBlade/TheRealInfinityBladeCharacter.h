// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "Sound/SoundCue.h"
#include "GameFramework/Character.h"
#include "TheRealInfinityBladeCharacter.generated.h"

UCLASS(config = Game)
class ATheRealInfinityBladeCharacter : public ACharacter
{
	GENERATED_BODY()

		/** Camera boom positioning the camera behind the character */
		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
public:
	ATheRealInfinityBladeCharacter();


	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Character)
		USphereComponent* trigger_box;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Character)
		float PlayerMaxHP = 100.0f;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Character)
		float PlayerMaxMP = 40.0f;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Character)
		float PlayerMaxRage = 100.0f;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Character)
		float PlayerCurrentHP;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Character)
		float PlayerCurrentMP;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Character)
		float PlayerCurrentRage;
	UENUM()
	enum RangeE {
		Fireball,
		Summon
	};
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Character)
		TEnumAsByte<RangeE> ranged_choice = Fireball;

protected:

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/**
	* Called via input to turn at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void TurnAtRate(float Rate);

	/**
	* Called via input to turn look up/down at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);
	

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface
	UPROPERTY(EditAnywhere, Category = Weapon)
		TSubclassOf<class AFlameSaber> FlameSaberClass;
	UPROPERTY(EditAnywhere, Category = Weapon)
		TSubclassOf<class ASummonedSwords> SummonClass;
	UPROPERTY(EditAnywhere, Category = Weapon)
		TSubclassOf<class ALeachField> LeachClass;
public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

public:
	UFUNCTION(BlueprintCallable, Category = "Damage")
    float TakeDamage(float Damage, FDamageEvent const& DamageEvent,
		AController* EventInstigator, AActor* DamageCauser) override;
	void Attackstart();
	void ZoomToEnemy();
	void CameraSnapBack();
	void Tick(float deltaTime)override;
	void DodgeTrue();
	void DodgeFalse();
	void Attack();
	void BeginPlay()override;
	void OnMouseWheelDown();
	void OnMouseWheelUp();
    void ActivateSuper(); 
	void RangeAttack();
	UFUNCTION()
	void OnTriggerActivate(AActor* other, UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex, bool FrontSweep,
		const FHitResult& Result);
	UFUNCTION()
		void OnTriggerDeActivate(class AActor* OtherActor, 
	class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	
    void RestoreHP(float restore); 
private:
	
    UPROPERTY(EditAnywhere)
        UAnimMontage* dash;
	UPROPERTY(EditAnywhere)
		UAnimMontage* dash_left;
	UPROPERTY(EditAnywhere)
		UAnimMontage* dash_right;
	UPROPERTY(EditAnywhere)
		UAnimMontage* dash_back;
	UPROPERTY(EditAnywhere)
		TArray<UAnimMontage*> attack_array;
	UPROPERTY(EditAnywhere)
		float dodgeDist = 10.0f;
	UPROPERTY(EditAnywhere)
		UAnimMontage* Range;
	
	UPROPERTY(EditAnywhere)
		float triggerRadius = 50.0f;
    //Combo System Stuff
	
    enum Combo
    {
        Attack1,
        Attack2a,
        Attack2b,
        Attack3a,
        Attack3b
        
    };

    int ComboState; //denotes which part of combo currently in
    FTimerHandle AttackResetTimer;
    FTimerHandle Attack2bTimer;
    void ChangeAttack2b() {ComboState = Attack2b;}
    void EndAttack();
    bool bIsSuperActive; 
    
    UPROPERTY(EditDefaultsOnly, Category = Sound)
    TArray<USoundCue*> PlayerDamageSFX;
    UPROPERTY(EditDefaultsOnly, Category = Sound) 
    TArray<USoundCue*> SwordSFXArray;
    UPROPERTY(EditDefaultsOnly, Category = Sound)
    TArray<USoundCue*> VoiceArray; 
    UAudioComponent* PlaySoundFX(USoundCue* cue);
    UAudioComponent* AttackAC;
    UAudioComponent* VoiceAC;
    //End Combo System Stuff
	
    
    
    bool bIsDead;
	bool bZoom = false;
	bool IsDodgeActive = false;
	int32 prev_index=0; 
	bool IsTrigger = false;
	
    class AEnemy* closestEnemy = nullptr;
	void UpdateRotation(float deltaTime);
	bool IsFocusActive = false;
	bool first = true;
	class APlayerWeapons* curr_weapon;
	class AFlameSaber* flame;
	class ASummonedSwords* summSword;
	TArray<class APlayerWeapons*> WeaponArray;
	unsigned int currentWeapon=0;
	FTimerHandle handle_attack;
	float OriginalCameraZ;
	UPROPERTY(EditDefaultsOnly)
	UAnimMontage* SuperKneel;
	UPROPERTY(EditDefaultsOnly)
	UParticleSystem* P_Range;
	UPROPERTY(EditDefaultsOnly)
	UAnimMontage* Death;
	UPROPERTY(EditDefaultsOnly)
	UParticleSystem* summonMark;
	UPROPERTY(EditDefaultsOnly)
	UAnimMontage* Summon_Anim;
	void PauseSkeleton();
	void UnPauseSkeleton();
	void RangeSet();
	void DetonateFireBall();
	void UpdateMP();
	void Recharge();
	void DeactivateMesh();
	UParticleSystemComponent* P_SummonMark;
	FVector LocateThroughControl();
	bool IsSummonActive = false;
    
    //Sound Effects Stuff
    UPROPERTY(EditDefaultsOnly, Category = Sound)
    USoundCue* RangedAttackSFX;
    UPROPERTY(EditDefaultsOnly, Category = Sound) 
    USoundCue* RangedAttackExplosionSFX;
    UPROPERTY(EditDefaultsOnly, Category = Sound)
    USoundCue* DashSFX;
    UPROPERTY(EditDefaultsOnly, Category = Sound)
    USoundCue* SuperVoice;
    UPROPERTY(EditDefaultsOnly, Category = Sound)
    USoundCue* DeathVoice;
    UPROPERTY(EditDefaultsOnly, Category = Sound)
    USoundCue* BackgroundMusic;
    UAudioComponent* BackgroundAC; 
    UPROPERTY(EditDefaultsOnly, Category = Sound)
    USoundCue* RangedExplosionVoice; 
	bool IsMPZero = false;
	
};



