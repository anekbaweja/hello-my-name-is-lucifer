// Fill out your copyright notice in the Description page of Project Settings.

#include "TheRealInfinityBlade.h"
#include "FlameSaber.h"
#include "Enemy.h"


AFlameSaber::AFlameSaber()
{
	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	RootComponent = WeaponMesh;
    BoxCollComp = CreateDefaultSubobject<UBoxComponent>(TEXT("FSBoxComponent"));

    bIsSuperActive = false;
    bIsWeaponActive= false; 
    SetActorEnableCollision(true);
    //WeaponMesh->SetSimulatePhysics(false);
	
	BoxCollComp->bGenerateOverlapEvents = false;
    BoxCollComp->MoveIgnoreActors.Add(WeaponOwner);
    BoxCollComp->MoveIgnoreActors.Add(this);
	BoxCollComp->AttachParent = RootComponent;
    SuperSphereRadius = 100.0f;
    SuperAttackTimer= 0.0f;
	sphere = CreateDefaultSubobject<USphereComponent>(TEXT("FSSuperSphere"));
	sphere->SetSphereRadius(SuperSphereRadius);
	
}

void AFlameSaber::OnStartAttack()
{
    
	BoxCollComp->bGenerateOverlapEvents = true;
	bIsWeaponActive = true;
	AttackAC= PlaySoundFX(AttackStartSoundFX);
    if(AttackAC)
    {
        AttackAC->Play();
    }
    
}

void AFlameSaber::OnEndAttack()
{
	bIsWeaponActive = false;
	BoxCollComp->bGenerateOverlapEvents = false;
}

void AFlameSaber::BeginPlay()
{
    BoxCollComp->OnComponentBeginOverlap.AddDynamic(this, &AFlameSaber::OnWeaponOverlapBegin);
	BoxCollComp->OnComponentEndOverlap.AddDynamic(this, &AFlameSaber::OnWeaponOverlapEnd);
	
	sphere->MoveIgnoreActors.Add(WeaponOwner);
	sphere->MoveIgnoreActors.Add(this);
	
}

void AFlameSaber::SuperAttack()
{
	bIsSuperActive = true;
	
	sphere->SetSphereRadius(SuperSphereRadius);
	sphere->AttachTo(WeaponOwner->GetRootComponent());
	sphere->bHiddenInGame = false;
    TArray<AActor*> enemiesInRange;
    sphere->GetOverlappingActors(enemiesInRange, EnemyClass);
    for(auto temp : enemiesInRange)
    {
        temp->TakeDamage(super_damage, FDamageEvent(), GetInstigatorController(), this);
    }
	if (first) {
        DamageAC = PlaySoundFX(SuperAttackSFX);
        if(DamageAC)
            DamageAC->Play();
        FVector vec = WeaponOwner->GetActorLocation();
		vec.Z -= 90.0f;
		UGameplayStatics::SpawnEmitterAtLocation(this, superball, vec);
		first = false;
	}
}

void AFlameSaber::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    if(bIsSuperActive)
    {
        if(SuperAttackTimer<= 1.5f)
        {
            SuperSphereRadius += 40 * DeltaTime;
            SuperAttackTimer+= DeltaTime;
			SuperAttack();
        }
        else
        {
            SuperSphereRadius = 100.0f;
            SuperAttackTimer= 0.0f;
			sphere->SetSphereRadius(0.0f);
			sphere->DetachFromParent();
            bIsSuperActive = false;
			first = true;
        }
    }
}
UAudioComponent* AFlameSaber::PlaySoundFX(USoundCue* cue)
{
    UAudioComponent* audio = nullptr;
    if(cue)
    {
        audio = UGameplayStatics::SpawnSoundAttached(cue, RootComponent);
    }
    return audio;
}


void AFlameSaber::OnWeaponOverlapBegin(AActor* other, UPrimitiveComponent* OtherComp,
                                       int32 OtherBodyIndex, bool FrontSweep,
                                       const FHitResult& result)
{
    bool NullChecks = this && other && OtherComp;
    if(NullChecks&&bIsWeaponActive)
    {
        
            AEnemy* enemy = Cast<AEnemy>(other);
			if (enemy)
			{
                int32 index = FMath::RandRange(0, EnemyDamageSFXArray.Num() -1);
                DamageAC = PlaySoundFX(EnemyDamageSFXArray[index]);
                if(DamageAC)
                    DamageAC->Play(); 
                enemy->TakeDamage(WeaponDamage, FDamageEvent(), GetInstigatorController(), this);
				//UGameplayStatics::SpawnEmitterAttached(sparks,OtherComp->GetAttachmentRoot());
				//UGameplayStatics::SpawnEmitterAtLocation(this, sparks, result.ImpactPoint);
				FHitResult Hit(ForceInit);
				GetWorld()->LineTraceSingleByObjectType(Hit, this->GetActorLocation(), OtherComp->GetComponentLocation(),
					FCollisionObjectQueryParams::AllDynamicObjects);
				UGameplayStatics::SpawnEmitterAtLocation(this, sparks, Hit.ImpactPoint);

			}
            
        
    }
}
void AFlameSaber::OnWeaponOverlapEnd(AActor* other, UPrimitiveComponent* OtherComp,
                                     int32 OtherBodyIndex)
{
	
	bIsWeaponActive = false;
	BoxCollComp->bGenerateOverlapEvents = false;
}

