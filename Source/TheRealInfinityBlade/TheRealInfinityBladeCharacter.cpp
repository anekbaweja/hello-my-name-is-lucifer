// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "TheRealInfinityBlade.h"
#include "TheRealInfinityBladeCharacter.h"
#include "Enemy.h"
#include "Kismet/KismetMathLibrary.h"
#include "FlameSaber.h"
#include "SummonedSwords.h"
#include "LeachField.h"
//////////////////////////////////////////////////////////////////////////
// ATheRealInfinityBladeCharacter

ATheRealInfinityBladeCharacter::ATheRealInfinityBladeCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
	RootComponent = GetCapsuleComponent();
	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->AttachTo(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

												// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->AttachTo(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

												   // Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
												   // are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
	PlayerCurrentHP = PlayerMaxHP;
	PlayerCurrentMP = 0.0f;
    PlayerCurrentRage = 0.0f;
	bIsDead = false;

	//Set up trigger box for Focus
	trigger_box = CreateDefaultSubobject<USphereComponent>(TEXT("TriggerBox"));
	SetActorEnableCollision(true);
	trigger_box->bGenerateOverlapEvents = true;
	trigger_box->MoveIgnoreActors.Add(this);
	trigger_box->InitSphereRadius(triggerRadius);
	trigger_box->AttachParent = GetMesh();
    bIsSuperActive = false;
}

//////////////////////////////////////////////////////////////////////////
// Input

void ATheRealInfinityBladeCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	// Set up gameplay key bindings
	check(InputComponent);
	//InputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	//InputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	InputComponent->BindAction("DodgeActive", IE_Pressed, this, &ATheRealInfinityBladeCharacter::DodgeTrue);
	//InputComponent->BindAction("DodgeActive", IE_Released, this, &ATheRealInfinityBladeCharacter::DodgeFalse);
	InputComponent->BindAxis("MoveForward", this, &ATheRealInfinityBladeCharacter::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ATheRealInfinityBladeCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	InputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	InputComponent->BindAxis("TurnRate", this, &ATheRealInfinityBladeCharacter::TurnAtRate);
	InputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	InputComponent->BindAxis("LookUpRate", this, &ATheRealInfinityBladeCharacter::LookUpAtRate);

	InputComponent->BindAction("Focus", IE_Pressed, this, &ATheRealInfinityBladeCharacter::ZoomToEnemy);
	InputComponent->BindAction("Focus", IE_Released, this, &ATheRealInfinityBladeCharacter::CameraSnapBack);

	InputComponent->BindAction("Attack", IE_Pressed, this, &ATheRealInfinityBladeCharacter::Attack);

	InputComponent->BindAction("Ranged", IE_Pressed, this, &ATheRealInfinityBladeCharacter::RangeAttack);
	//InputComponent->BindAction("Ranged", IE_Released, this, &ATheRealInfinityBladeCharacter::UnPauseForRange);

	InputComponent->BindAction("MouseUp", IE_Pressed, this, &ATheRealInfinityBladeCharacter::OnMouseWheelUp);
	InputComponent->BindAction("MouseDown", IE_Pressed, this, &ATheRealInfinityBladeCharacter::OnMouseWheelDown);
    InputComponent->BindAction("SuperAttack", IE_Pressed, this, &ATheRealInfinityBladeCharacter::ActivateSuper);
	InputComponent->BindAction("Detonate", IE_Pressed, this, &ATheRealInfinityBladeCharacter::DetonateFireBall);
    //InputComponent->BindAction("Dodge", IE_Pressed, this, &ATheRealInfinityBladeCharacter::Dodge);
	//InputComponent->BindAction("Dodge", IE_Released, this, &ATheRealInfinityBladeCharacter::EndDodge);
	// handle touch devices
	//InputComponent->BindTouch(IE_Pressed, this, &ATheRealInfinityBladeCharacter::TouchStarted);
	//InputComponent->BindTouch(IE_Released, this, &ATheRealInfinityBladeCharacter::TouchStopped);
	WeaponArray.Add(flame);
}


void ATheRealInfinityBladeCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	// jump, but only on the first touch
	if (FingerIndex == ETouchIndex::Touch1)
	{
		Jump();
	}
}

void ATheRealInfinityBladeCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	if (FingerIndex == ETouchIndex::Touch1)
	{
		StopJumping();
	}
}

void ATheRealInfinityBladeCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ATheRealInfinityBladeCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ATheRealInfinityBladeCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
			
			if (IsDodgeActive) {
				Direction *= Value;
				//Direction *= dodgeDist;
				FVector pos = UGameplayStatics::GetPlayerPawn(this, 0)->GetActorLocation();
				//FVector forward = UGameplayStatics::GetPlayerPawn(this, 0)->GetActorForwardVector();
				FVector endpos = pos + Direction*dodgeDist;
				static FName DodgeTrace = FName(TEXT("DodgeTrace"));
				FCollisionQueryParams TraceParams(DodgeTrace, true, Instigator);
				TraceParams.bTraceAsyncScene = true;
				TraceParams.bReturnPhysicalMaterial = true;
				FHitResult Hit(ForceInit);
				GetWorld()->LineTraceSingleByObjectType(Hit, pos, endpos,
				FCollisionObjectQueryParams::AllObjects, TraceParams);
				if (!Hit.bBlockingHit) {
				UGameplayStatics::GetPlayerPawn(this, 0)->SetActorLocation(endpos);
				}
				if (IsFocusActive&&closestEnemy) {
					if (Value > 0) { PlayAnimMontage(dash); }
					else { PlayAnimMontage(dash_back); }
				}
				else {PlayAnimMontage(dash);}
			}
			
			else{AddMovementInput(Direction, Value);}
	}

}

void ATheRealInfinityBladeCharacter::MoveRight(float Value)
{

	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		if (IsDodgeActive) {
			Direction *= Value;
			//Direction *= dodgeDist;
			FVector pos = UGameplayStatics::GetPlayerPawn(this, 0)->GetActorLocation();
			//FVector forward = UGameplayStatics::GetPlayerPawn(this, 0)->GetActorForwardVector();
			FVector endpos = pos + Direction*dodgeDist;
			static FName DodgeTrace = FName(TEXT("DodgeTrace"));
			FCollisionQueryParams TraceParams(DodgeTrace, true, Instigator);
			TraceParams.bTraceAsyncScene = true;
			TraceParams.bReturnPhysicalMaterial = true;
			FHitResult Hit(ForceInit);
			GetWorld()->LineTraceSingleByObjectType(Hit, pos, endpos,
				FCollisionObjectQueryParams::AllObjects, TraceParams);
			if (!Hit.bBlockingHit) {
				UGameplayStatics::GetPlayerPawn(this, 0)->SetActorLocation(endpos);
			}
			if (IsFocusActive&&closestEnemy){
				if (Value > 0) { PlayAnimMontage(dash_right); }
				else { PlayAnimMontage(dash_left); }
			}
			else{
			PlayAnimMontage(dash);}
		}
		else { AddMovementInput(Direction, Value); }

	}

}

float ATheRealInfinityBladeCharacter::TakeDamage(float Damage, FDamageEvent const& DamageEvent,
	AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
	if (ActualDamage > 0.0f)
	{
		
        
        if(!bIsSuperActive)
        {
            PlayerCurrentHP -= ActualDamage;
        
        }

		if (PlayerCurrentHP <= 0.0f)
		{

			bCanBeDamaged = false;
			//OnEndFire();
			bIsDead = true;
			APlayerController* PC = Cast<APlayerController>(GetController());
			if (PC)
			{
				PC->SetCinematicMode(true, true, true);
			}
			//float animDuration=PlayAnimMontage(DeathAnim);
			FTimerHandle deactivateMeshTimer;
			//GetWorldTimerManager().SetTimer(deactivateMeshTimer, this, &Alab5Character::DeactivateMesh, animDuration-0.1f);
            VoiceAC = PlaySoundFX(DeathVoice);
            if(VoiceAC)
                VoiceAC->Play(); 
            if(BackgroundAC)
                BackgroundAC->Stop(); 
           float dur =  PlayAnimMontage(Death);
		   GetWorldTimerManager().SetTimer(deactivateMeshTimer, this, &ATheRealInfinityBladeCharacter::DeactivateMesh, dur);
		}
        else
        {
            
            int index = FMath::RandRange(0, PlayerDamageSFX.Num()-1);
            UAudioComponent* temp = PlaySoundFX(PlayerDamageSFX[index]);
            if(temp)
                temp->Play();
            
        }
	}
	return ActualDamage;
}
void ATheRealInfinityBladeCharacter::DeactivateMesh() {
	GetMesh()->Deactivate();
	APlayerController* PC = Cast<APlayerController>(GetController());	PC->ConsoleCommand(TEXT("RestartLevel"));
}
void ATheRealInfinityBladeCharacter::Attackstart()
{
	GetWorld()->GetTimerManager().ClearTimer(handle_attack);
	curr_weapon->OnStartAttack();
}


void ATheRealInfinityBladeCharacter::ZoomToEnemy()
{
	IsFocusActive = true;
	//bZoom = true;
	//CameraBoom->TargetArmLength = 120.0f;
	first = true;
}

void ATheRealInfinityBladeCharacter::CameraSnapBack()
{
	IsFocusActive = false;
	closestEnemy = nullptr;
	//bZoom = false;
	//CameraBoom->TargetArmLength = 300.0f;
}

void ATheRealInfinityBladeCharacter::Tick(float deltaTime)
{
	if (bZoom) { CameraBoom->TargetArmLength = FMath::Lerp(CameraBoom->TargetArmLength, 120.0f, deltaTime * 10.0f); }
	else { CameraBoom->TargetArmLength = FMath::Lerp(CameraBoom->TargetArmLength, 300.0f, deltaTime * 10.0f); }

	if (IsFocusActive&&closestEnemy) {
		UpdateRotation(deltaTime);
	}

	if (IsSummonActive) 
	{ 
		FVector temp = LocateThroughControl(); 
		P_SummonMark->SetWorldLocation(temp);
		
	}
	if (PlayerCurrentMP<=0.0f) {
		UpdateMP();
	}
}
void ATheRealInfinityBladeCharacter::UpdateMP() {
	if (PlayerCurrentMP == PlayerMaxMP) { return;  }
	FTimerHandle handleMP;
	GetWorldTimerManager().SetTimer(handleMP, this, &ATheRealInfinityBladeCharacter::UpdateMP, 1.0f, false);
	PlayerCurrentMP += 5.0f;
}
void ATheRealInfinityBladeCharacter::DodgeTrue()
{
    UAudioComponent* temp = PlaySoundFX(DashSFX);
    if(temp)
        temp->Play();
    IsDodgeActive = true;

	FTimerHandle handle;
	GetWorldTimerManager().SetTimer(handle, this, &ATheRealInfinityBladeCharacter::DodgeFalse,
		0.1f, false);
}

void ATheRealInfinityBladeCharacter::DodgeFalse()
{
	IsDodgeActive = false;
}

void ATheRealInfinityBladeCharacter::Attack()
{
    float dur= 0.0f;
    bool bCanAttack = bIsSuperActive || bIsDead;
    switch (ComboState)
    {
        case Attack1:
        {
            if(!bCanAttack)
            {
                AttackAC = PlaySoundFX(SwordSFXArray[0]);
                if(AttackAC)
                    AttackAC->Play();
                VoiceAC = PlaySoundFX(VoiceArray[0]);
                if(VoiceAC)
                    VoiceAC->Play();
            }
            dur = PlayAnimMontage(attack_array[0]);
            GetWorldTimerManager().SetTimer(AttackResetTimer, this,
                                            &ATheRealInfinityBladeCharacter::EndAttack, dur+0.5f);
            ComboState = Attack2a;
            GetWorldTimerManager().SetTimer(Attack2bTimer, this,
                                            &ATheRealInfinityBladeCharacter::ChangeAttack2b, dur-0.1f);
            
            break;
        }
            
        case Attack2a:
        {
            
            if(!bCanAttack)
            {
                AttackAC = PlaySoundFX(SwordSFXArray[1]);
                if(AttackAC)
                    AttackAC->Play();
                VoiceAC = PlaySoundFX(VoiceArray[1]);
                if(VoiceAC)
                    VoiceAC->Play();
            }
            
            GetWorldTimerManager().ClearTimer(Attack2bTimer);
            GetWorldTimerManager().ClearTimer(AttackResetTimer);
            dur = PlayAnimMontage(attack_array[1]);
            GetWorldTimerManager().SetTimer(AttackResetTimer, this,
                                            &ATheRealInfinityBladeCharacter::EndAttack, dur+0.5f);
            ComboState = Attack3a;
            break;
        }
            
        case Attack2b:
        {
            if(!bCanAttack)
            {
                AttackAC = PlaySoundFX(SwordSFXArray[2]);
                if(AttackAC)
                    AttackAC->Play();
                VoiceAC = PlaySoundFX(VoiceArray[2]);
                if(VoiceAC)
                    VoiceAC->Play();
            }
            GetWorldTimerManager().ClearTimer(Attack2bTimer);
            dur = PlayAnimMontage(attack_array[2]);
            GetWorldTimerManager().SetTimer(AttackResetTimer, this,
                                            &ATheRealInfinityBladeCharacter::EndAttack, dur+0.5f);
            ComboState = Attack3b;
            break;
        }
        case Attack3a:
        {
            if(!bCanAttack)
            {
                AttackAC = PlaySoundFX(SwordSFXArray[3]);
                if(AttackAC)
                    AttackAC->Play();
                VoiceAC = PlaySoundFX(VoiceArray[3]);
                if(VoiceAC)
                    VoiceAC->Play();
            }
            GetWorldTimerManager().ClearTimer(AttackResetTimer);
            dur = PlayAnimMontage(attack_array[3]);
            EndAttack();
            ComboState = Attack1;
            break;
        }
            
        case Attack3b:
        {
            
            if(!bCanAttack)
            {
                AttackAC = PlaySoundFX(SwordSFXArray[4]);
                if(AttackAC)
                    AttackAC->Play();
                VoiceAC = PlaySoundFX(VoiceArray[4]);
                if(VoiceAC)
                    VoiceAC->Play();
            }
            dur = PlayAnimMontage(attack_array[4]);
            EndAttack();
            ComboState = Attack1;
            break;
        }
            
        default:
            break;
            
    }
    
    APlayerController* PC = Cast<APlayerController>(GetController());
    //prev_index = index;
    
    GetWorldTimerManager().SetTimer(handle_attack, this, &ATheRealInfinityBladeCharacter::Attackstart,
                                    dur-0.2f, false);
}

void ATheRealInfinityBladeCharacter::BeginPlay() {
	trigger_box->OnComponentBeginOverlap.AddDynamic(this, &ATheRealInfinityBladeCharacter::OnTriggerActivate);
	trigger_box->OnComponentEndOverlap.AddDynamic(this, &ATheRealInfinityBladeCharacter::OnTriggerDeActivate);
    BackgroundAC = PlaySoundFX(BackgroundMusic);
    if(BackgroundAC)
        BackgroundAC->Play();
    if (FlameSaberClass)
	{
		UWorld* World = GetWorld();
		if (World)
		{
			FActorSpawnParameters SpawnParams;
			SpawnParams.Owner = this;
			SpawnParams.Instigator = Instigator;
			// Spawn the Weapon 
			WeaponArray[currentWeapon] = World->SpawnActor<AFlameSaber>(FlameSaberClass, FVector::ZeroVector,
				FRotator(200.0f, 0.0f, 0.0f), SpawnParams);
			if (WeaponArray[currentWeapon])
			{
				WeaponArray[currentWeapon]->AttachRootComponentTo(GetMesh(), TEXT("Weapon_L"));
				curr_weapon = WeaponArray[currentWeapon];
			}
		}
		curr_weapon->SetWeaponOwner(this);
	}
}

void ATheRealInfinityBladeCharacter::OnMouseWheelDown()
{
	ranged_choice = Summon;
	
}

void ATheRealInfinityBladeCharacter::OnMouseWheelUp()
{
	ranged_choice = Fireball;
	if (IsSummonActive) { IsSummonActive = false; P_SummonMark->Deactivate(); }
}


void ATheRealInfinityBladeCharacter::OnTriggerActivate(AActor * other, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool FrontSweep, const FHitResult & Result)
{
	bool bNullChecks = (this) && (other) && (other != this);
	if (bNullChecks)
	{
		AEnemy* enemy = &*(Cast<AEnemy>(other));
		if(enemy)
		{
			closestEnemy = &*enemy;
			IsTrigger = true;
		}
	}
}

void ATheRealInfinityBladeCharacter::OnTriggerDeActivate(class AActor* OtherActor,class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	//closestEnemy = nullptr;
}



void ATheRealInfinityBladeCharacter::UpdateRotation(float deltaTime)
{
	//if (!closestEnemy) { return; }
	AController* cont = UGameplayStatics::GetPlayerController(this, 0);
	FRotator pitch = GetActorRotation();
	FRotator Rot = UKismetMathLibrary::FindLookAtRotation(this->GetMesh()->GetComponentLocation(), closestEnemy->GetActorLocation());
	Rot.Pitch = pitch.Pitch;
	if(first){
		cont->SetControlRotation(FMath::RInterpTo(GetActorRotation(), Rot, deltaTime, deltaTime*30.0f));
		this->SetActorRelativeRotation(FMath::RInterpTo(GetActorRotation(), Rot, deltaTime, deltaTime*10.0f));
		first = false;
	}
	else {
		cont->SetControlRotation(Rot);
		this->SetActorRelativeRotation(Rot);
	}
    


	/*AController* cont = UGameplayStatics::GetPlayerController(this, 0);
	FVector enemyPos = closestEnemy->GetActorLocation();
	FVector myPos = this->GetActorLocation();
	FVector dir = enemyPos - myPos;
	dir.Normalize();
	float angle = FVector::DotProduct(dir,this->GetActorForwardVector());
	FVector crossProd = FVector::CrossProduct(this->GetActorForwardVector() , dir);
	if (crossProd.Z > 0) { angle *= -1; }
	FQuat rot(this->GetActorUpVector(), angle);
	FRotator Frot(rot);
	if (crossProd.X == 0 && crossProd.Y == 0 && crossProd.Z == 0) 
	{
		APawn* pawn = UGameplayStatics::GetPlayerPawn(this, 0);
		//FRotator rots = pawn->GetActorRotation().Add(FRotator(FQuat::Identity).Pitch, FRotator(FQuat::Identity).Yaw, FRotator(FQuat::Identity).Roll);
		cont->SetControlRotation(pawn->GetActorRotation()+FRotator(FQuat::Identity));
		pawn->SetActorRotation(pawn->GetActorRotation() + FRotator(FQuat::Identity));
	}
	else {
		APawn* pawn = UGameplayStatics::GetPlayerPawn(this, 0);
		//FRotator rots = pawn->GetActorRotation().Add(Frot.Pitch, Frot.Yaw, Frot.Roll);
		cont->SetControlRotation(pawn->GetActorRotation()+Frot);
		pawn->SetActorRotation(pawn->GetActorRotation() +Frot);
	}*/
	
}

void ATheRealInfinityBladeCharacter::RestoreHP(float restore)
{
    float temp= PlayerCurrentHP + restore;
    if(temp < PlayerMaxHP)
    {
        PlayerCurrentHP = temp;
    }
    
    else
    {
        PlayerCurrentHP = PlayerMaxHP;
    }
}

void ATheRealInfinityBladeCharacter::ActivateSuper()
{
	if (PlayerCurrentMP >= 10.0f) {
	bIsSuperActive = true;
	VoiceAC = PlaySoundFX(SuperVoice);
	if (VoiceAC)
		VoiceAC->Play();
	float dur = PlayAnimMontage(SuperKneel);
	FTimerHandle handle;
	GetWorldTimerManager().SetTimer(handle, this, &ATheRealInfinityBladeCharacter::PauseSkeleton,
		dur - 0.19f, false); PlayerCurrentMP -= 10.0f;
	if (PlayerCurrentMP <= 0) { IsMPZero = true; }
}
	else { return; }
}
void ATheRealInfinityBladeCharacter::PauseSkeleton()
{
	GetMesh()->bNoSkeletonUpdate = true;
	
	FTimerHandle handle;
	GetWorldTimerManager().SetTimer(handle, this, &ATheRealInfinityBladeCharacter::UnPauseSkeleton,
		1.0f, false);
}
void ATheRealInfinityBladeCharacter::UnPauseSkeleton()
{
	curr_weapon->SuperAttack();
	GetMesh()->bNoSkeletonUpdate = false;
    bIsSuperActive = false;
    
}

void ATheRealInfinityBladeCharacter::RangeAttack()
{
	if (ranged_choice == Fireball) {
        UAudioComponent* temp = PlaySoundFX(RangedAttackSFX);
        if (temp)
            temp->Play();
        float dur = PlayAnimMontage(Range);
		FTimerHandle handle;
		GetWorldTimerManager().SetTimer(handle, this, &ATheRealInfinityBladeCharacter::RangeSet,
			dur, false);
	}
	else {
		//Spawn locator particle effect at location
		//call LocateThroughControl()
		if (!IsSummonActive) {
			IsSummonActive = true;
			FVector temp = LocateThroughControl();
			P_SummonMark = UGameplayStatics::SpawnEmitterAtLocation(this, summonMark, temp);
		}
	}
}
FVector ATheRealInfinityBladeCharacter::LocateThroughControl()
{
	AController* cont = GetController();
	APlayerController* Cont = Cast<APlayerController>(cont);
	if (Cont) {
		FHitResult result;
		Cont->GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility,true,result);
		FVector temp = result.ImpactPoint;
		temp.Z = this->GetActorLocation().Z; 
		return temp;
	}
	return FVector::ZeroVector;
}

void ATheRealInfinityBladeCharacter::RangeSet() {
	if (SummonClass)
	{
		UWorld* World = GetWorld();
		if (World)
		{
			FActorSpawnParameters SpawnParams;
			SpawnParams.Owner = this;
			SpawnParams.Instigator = Instigator;
			// Spawn the Weapon 
			FVector loc = GetMesh()->GetSocketLocation(TEXT("Weapon_R"));
			ASummonedSwords* range = World->SpawnActor<ASummonedSwords>(SummonClass, loc,
				FRotator(0.0f, 0.0f, 0.0f), SpawnParams);
			if (range)
			{
				range->SetWeaponOwner(this);
				range->InitVelocity(this->GetActorRotation().Vector());
			}
		}
	}
}

void ATheRealInfinityBladeCharacter::DetonateFireBall()
{
	if (PlayerCurrentMP >= 10.0f) {
	if (ranged_choice == Fireball) {
		TArray<AActor*> retValue;
		if (retValue.Num() > 0)
		{
			AttackAC = PlaySoundFX(RangedAttackExplosionSFX);
			if (AttackAC)
				AttackAC->Play();
			VoiceAC = PlaySoundFX(RangedExplosionVoice);
			if (VoiceAC)
				VoiceAC->Play();
		}
		UGameplayStatics::GetAllActorsOfClass(this, SummonClass, retValue);
		for (auto em : retValue) {
			ASummonedSwords* ball = Cast<ASummonedSwords>(em);
			if (ball)
			{
				UAudioComponent* temp = PlaySoundFX(RangedAttackExplosionSFX);
				if (temp)
					temp->Play();
				ball->Detonate();
			}
		}
		PlayerCurrentMP -= 10.0f;
		if (PlayerCurrentMP <= 0) { IsMPZero = true; }
	}
	else {
		if (LeachClass&&IsSummonActive)
		{
			UWorld* World = GetWorld();
			if (World)
			{
				FActorSpawnParameters SpawnParams;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = Instigator;
				// Spawn the Weapon 
				FVector loc = P_SummonMark->GetComponentLocation();
				loc.Z -= 85.0f;
				ALeachField* range = World->SpawnActor<ALeachField>(LeachClass, loc,
					FRotator(0.0f, 0.0f, 0.0f), SpawnParams);
				if (range)
				{
					PlayAnimMontage(Summon_Anim);
					IsSummonActive = false;
					P_SummonMark->DestroyComponent();
					range->SetFieldOwner(this);
				}
			}
		}
		PlayerCurrentMP -= 10.0f;
		if (PlayerCurrentMP <= 0) { IsMPZero = true; }
	}
}
}

UAudioComponent* ATheRealInfinityBladeCharacter::PlaySoundFX(USoundCue* cue)
{
    UAudioComponent* audio = nullptr;
    if(cue)
    {
        audio = UGameplayStatics::SpawnSoundAttached(cue, RootComponent);
    }
    
    return audio;
}

void ATheRealInfinityBladeCharacter::EndAttack()
{
    ComboState = Attack1; 
    curr_weapon->OnEndAttack();
}