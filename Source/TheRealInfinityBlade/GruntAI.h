// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Enemy.h"
#include "GruntAI.generated.h"

UCLASS()
class THEREALINFINITYBLADE_API AGruntAI : public AEnemy
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AGruntAI();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

private:

	
};
