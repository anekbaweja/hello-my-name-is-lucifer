// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "LeachField.generated.h"

UCLASS()
class THEREALINFINITYBLADE_API ALeachField : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALeachField();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	void SetFieldOwner(AActor* inActor) { WeaponOwner = inActor;  }
	void Death();
	void Leach();
private:
	UPROPERTY(EditDefaultsOnly)
	UParticleSystem* P_Field;
	UPROPERTY(EditDefaultsOnly)
	USphereComponent* SphereComp;
	AActor* WeaponOwner;
	FTimerHandle leachTimer;
};
