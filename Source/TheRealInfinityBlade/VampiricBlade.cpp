// Fill out your copyright notice in the Description page of Project Settings.

#include "TheRealInfinityBlade.h"
#include "VampiricBlade.h"
#include "TheRealInfinityBladeCharacter.h"
#include "Enemy.h"


AVampiricBlade::AVampiricBlade()
{
    BoxCollComp = CreateDefaultSubobject<UBoxComponent>(TEXT("FSBoxComponent"));
    bIsSuperActive = false;
    bIsWeaponActive= false;
    SetActorEnableCollision(true);
    //WeaponMesh->SetSimulatePhysics(false);
    BoxCollComp->bGenerateOverlapEvents=true;
    BoxCollComp->MoveIgnoreActors.Add(WeaponOwner);
    BoxCollComp->MoveIgnoreActors.Add(this);
    SuperSphereRadius = 100.0f;
    SuperAttackTimer= 0.0f;
}

void AVampiricBlade::OnStartAttack()
{
    bIsWeaponActive=true;
    AttackAC= PlaySoundFX(AttackStartSoundFX);
    if(AttackAC)
    {
        AttackAC->Play();
    }
    
}

void AVampiricBlade::OnEndAttack()
{
    bIsWeaponActive=false;
}

void AVampiricBlade::BeginPlay()
{
    
    BoxCollComp->OnComponentBeginOverlap.AddDynamic(this, &AVampiricBlade::OnWeaponOverlapEnd);
    
}

//20% damage dealt using this weapon is restored as health
void AVampiricBlade::SuperAttack()
{
    bIsSuperActive=true;
    
}

void AVampiricBlade::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    if(bIsSuperActive)
    {
        if(SuperAttackTimer<= 5.0f)
        {
            SuperAttackTimer+= DeltaTime;
        }
        else
        {
            bIsSuperActive = false;
            SuperAttackTimer= 0.0f; 
        }
    }
}
UAudioComponent* AVampiricBlade::PlaySoundFX(USoundCue* cue)
{
    UAudioComponent* audio = nullptr;
    if(cue)
    {
        audio = UGameplayStatics::SpawnSoundAttached(cue, RootComponent);
    }
    return audio;
}

void AVampiricBlade::OnHit(AActor* selfActor, AActor* otherActor,
                        FVector NoemalImpulse, const FHitResult& Hit)
{
    bool bNullChecks= (selfActor) && (otherActor) && (otherActor != this)
    && (otherActor!=(GetWeaponOwner()));
    if(bNullChecks)
    {
        
    }
}

void AVampiricBlade::OnWeaponOverlapBegin(AActor* other, UPrimitiveComponent* OtherComp,
                                       int32 OtherBodyIndex, bool FrontSweep,
                                       const FHitResult& result)
{
    bool NullChecks = this && other && OtherComp;
    if(NullChecks)
    {
        if(bIsWeaponActive)
        {
            AEnemy* enemy= Cast<AEnemy>(other);
            float damage= enemy->TakeDamage(WeaponDamage, FDamageEvent(), GetInstigatorController(), this);
            if(bIsSuperActive)
            {
                ATheRealInfinityBladeCharacter* characr
                = Cast<ATheRealInfinityBladeCharacter>(WeaponOwner);
                //character->RestoreHP(damage/10.0);
            }
        }
    }
}
void AVampiricBlade::OnWeaponOverlapEnd(AActor* other, UPrimitiveComponent* OtherComp,
                                     int32 OtherBodyIndex, bool FrontSweep,
                                     const FHitResult& result)
{
    //do domething
}






