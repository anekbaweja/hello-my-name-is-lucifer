// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PlayerWeapons.h"
#include "Sound/SoundCue.h" 
#include "VampiricBlade.generated.h"

/**
 * 
 */
UCLASS()
class THEREALINFINITYBLADE_API AVampiricBlade : public APlayerWeapons
{
	GENERATED_BODY()
public:
    AVampiricBlade();
    void OnStartAttack() override;
    void OnEndAttack() override;
    void BeginPlay() override;
    void SuperAttack() override;
    UAudioComponent* PlaySoundFX(USoundCue* cue);
    void Tick(float DeltaTime) override;
    void OnHit(AActor* selfActor, AActor* otherActor,
               FVector NoemalImpulse, const FHitResult& Hit);
    UFUNCTION()
    void OnWeaponOverlapBegin(AActor* other, UPrimitiveComponent* OtherComp,
                              int32 OtherBodyIndex, bool FrontSweep,
                              const FHitResult& Result);
    
    UFUNCTION()
    void OnWeaponOverlapEnd(AActor* other, UPrimitiveComponent* OtherComp,
                            int32 OtherBodyIndex, bool fromSweep,
                            const FHitResult& result);
private:
    //All Sound Effects n Stuff
    UPROPERTY(Transient)
    UAudioComponent* AttackAC;
    UPROPERTY(EditDefaultsOnly, Category= Sound)
    USoundCue* AttackStartSoundFX;
    UPROPERTY(EditDefaultsOnly, Category= Sound)
    USoundCue* AttackEndSoundFX;
    UPROPERTY(EditDefaultsOnly)
    UBoxComponent* BoxCollComp;
    bool bIsSuperActive;
    float SuperSphereRadius; //the coll sphere of the super attack
    float SuperAttackTimer;
private:
    //All ParticleEffects and Stuff
    
private:
    //All animation montages
    
    

};
