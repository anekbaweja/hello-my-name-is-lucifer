// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Sound/SoundCue.h"
#include "PlayerWeapons.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "SummonedSwords.generated.h"

/**
 *
 */
UCLASS()
class THEREALINFINITYBLADE_API ASummonedSwords : public APlayerWeapons
{
    GENERATED_BODY()
public:
    ASummonedSwords();
    void OnStartAttack() override;
    void BeginPlay() override;
    void SuperAttack() override;
	void Tick(float deltaTime)override;
    UAudioComponent* PlaySoundFX(USoundCue* cue);
    float GetFireRate() {return FireRate;}
	void InitVelocity(const FVector& ShootDirection);
	void Detonate();
	
private:
    //All the Sounds and AudioComponents
    UPROPERTY(Transient)
    UAudioComponent* AttackAC;
    UPROPERTY(EditDefaultsOnly, Category= Sound)
    USoundCue* AttackStartFX;
    UPROPERTY(EditDefaultsOnly, Category = Sound)
    USoundCue* AttackImpactFX;
protected:
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Weapon)
		USkeletalMeshComponent* WeaponMesh;
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Weapon)
	UProjectileMovementComponent* MoveComp;
private:
    UFUNCTION()
    void OnWeaponOverlapBegin(AActor* other, UPrimitiveComponent* OtherComp,
                              int32 OtherBodyIndex, bool fromSweep,
                              const FHitResult& result);
    UFUNCTION()
    void OnWeaponOverlapEnd(AActor* other, UPrimitiveComponent* OtherComp,
                            int32 OtherBodyIndex, bool fromSweep,
                            const FHitResult& result);
	UFUNCTION()
		void OnHit(AActor* OtherActor, UPrimitiveComponent* OtherComp, 
			FVector NormalImpulse, const FHitResult& Hit);
private:
    //All particle effects and stuff

    
private:
    //All animated montages
	UPROPERTY(EditDefaultsOnly)
		UParticleSystem* impact_spark;
	UPROPERTY(EditDefaultsOnly)
		UParticleSystem* P_trail;
	UPROPERTY(EditDefaultsOnly)
		UParticleSystem* DetonateExplosion;
	UParticleSystemComponent* result_P; 
	
	
private:
    UPROPERTY(EditDefaultsOnly)
    float FireRate; //wait time between swords
	UPROPERTY(EditDefaultsOnly)
		USphereComponent* BoxCollComp;
	bool IsDetonateActive = false;
	bool PressedE = false;
	float DetTimer = 0.0f;
    
    //Audio Stuff
    UAudioComponent* DamagedAC;
    UPROPERTY(EditDefaultsOnly, Category=Sound)
    TArray<USoundCue*> DamagedSFXArray;
};
