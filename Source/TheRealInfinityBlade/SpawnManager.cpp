// Fill out your copyright notice in the Description page of Project Settings.

#include "TheRealInfinityBlade.h"
#include "SpawnManager.h"
#include "Engine/TargetPoint.h"


// Sets default values
ASpawnManager::ASpawnManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpawnManager::BeginPlay()
{
	Super::BeginPlay();
	OnSpawnTimer();
	FTimerHandle SpawnTimer;
	GetWorldTimerManager().SetTimer(SpawnTimer, this, &ASpawnManager::OnSpawnTimer, SpawnTime, true);
}

// Called every frame
void ASpawnManager::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void ASpawnManager::OnSpawnTimer() {
	int spawnNum = SpawnPoints.Num();

	if (spawnNum > 0) {

		if (type == 1) {
			switch (wave) {
			case 1:
				spawnNumber = 1;
				break;
			case 2:
				spawnNumber = 2;
				break;
			case 3:
				spawnNumber = 2;
				break;
			case 4:
				spawnNumber = 0;
				break;
			case 5:
				spawnNumber = 2;
				break;
			case 6:
				spawnNumber = 0;
				break;
			default:
				spawnNumber = 10;
			}
		}
		if (type == 2) {
			switch (wave) {
			case 1:
				spawnNumber = 0;
				break;
			case 2:
				spawnNumber = 0;
				break;
			case 3:
				spawnNumber = 1;
				break;
			case 4:
				spawnNumber = 3;
				break;
			case 5:
				spawnNumber = 2;
				break;
			case 6:
				spawnNumber = 0;
				break;
			default:
				spawnNumber = 10;
			}
		}
		if (type == 3) {
			switch (wave) {
			case 1:
				spawnNumber = 0;
				break;
			case 2:
				spawnNumber = 0;
				break;
			case 3:
				spawnNumber = 0;
				break;
			case 4:
				spawnNumber = 0;
				break;
			case 5:
				spawnNumber = 0;
				break;
			case 6:
				spawnNumber = 1;
				bossSpawned = true;
				break;
			default:
				spawnNumber = 2;
			}
		}

		for (int i = 0; i < spawnNumber; ++i) {

			int rand = FMath::RandRange(0, spawnNum - 1);
			FVector pos = SpawnPoints[rand]->GetActorLocation();
			FRotator rot = SpawnPoints[rand]->GetActorRotation();

			ACharacter* Char = GetWorld()->SpawnActor<ACharacter>(CharacterClass, pos, rot);
			if (Char)
			{
				Char->SpawnDefaultController();
			}
		}
		wave++;	
		//useless comment
	}
}



