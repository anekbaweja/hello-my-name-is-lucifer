// Fill out your copyright notice in the Description page of Project Settings.

#include "TheRealInfinityBlade.h"
#include "LeachField.h"
#include "Enemy.h"
#include "TheRealInfinityBladeCharacter.h"


// Sets default values
ALeachField::ALeachField()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SSphereComp"));
	SphereComp->SetSimulatePhysics(false);
	SphereComp->MoveIgnoreActors.Add(this);
	SphereComp->MoveIgnoreActors.Add(WeaponOwner);
	SphereComp->SetSphereRadius(120.0f);
	RootComponent = SphereComp;
	SphereComp->bHiddenInGame = false;
}

// Called when the game starts or when spawned
void ALeachField::BeginPlay()
{
	Super::BeginPlay();
	UGameplayStatics::SpawnEmitterAttached(P_Field, RootComponent)->SetRelativeScale3D(FVector(0.5f, 0.5f, 0.5f));
	FTimerHandle handle;
	GetWorldTimerManager().SetTimer(handle, this, &ALeachField::Death, 5.0f);
	GetWorldTimerManager().SetTimer(leachTimer, this, &ALeachField::Leach, 0.5f,true);
}

// Called every frame
void ALeachField::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ALeachField::Death()
{
	this->Destroy(); 
}

void ALeachField::Leach()
{
	//GetWorldTimerManager().ClearTimer(leachTimer);
	TArray<AActor*> overlap;
	SphereComp->GetOverlappingActors(overlap);
	for (auto temp : overlap)
	{
		AEnemy* enemy = Cast<AEnemy>(temp);
		if (enemy)
		{
			enemy->TakeDamage(5.0f, FDamageEvent(), GetInstigatorController(), this);
			ATheRealInfinityBladeCharacter* character = Cast<ATheRealInfinityBladeCharacter>(WeaponOwner);
			if (character) { character->RestoreHP(5.0f); }
			//enemy->GetCharacterMovement()->MaxWalkSpeed = 100.0f;
		}
	}
}

