// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "TheRealInfinityBladeGameMode.generated.h"

UCLASS(minimalapi)
class ATheRealInfinityBladeGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ATheRealInfinityBladeGameMode();
};



