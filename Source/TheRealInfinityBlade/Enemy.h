// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "Enemy.generated.h"

UCLASS()
class THEREALINFINITYBLADE_API AEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	float TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	void Death();

protected:

	UPROPERTY(EditDefaultsOnly, Category = Animation)
		UAnimMontage* KnockBackAnim;

	UPROPERTY(EditDefaultsOnly, Category = Animation)
		UAnimMontage* DeathAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Damage)
		float Health;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Damage)
		float Damage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Damage)
		bool isAttacking;

	UPROPERTY(EditDefaultsOnly, Category = Damage)
		float DeathTime;

};