// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Sound/SoundCue.h"
#include "PlayerWeapons.h"
#include "FlameSaber.generated.h"

/** This weapon is the first weapoin of the player, default weapon
 * Super attack is a wave of flames that spread out
 */
UCLASS()
class THEREALINFINITYBLADE_API AFlameSaber : public APlayerWeapons
{
    GENERATED_BODY()
public:
    AFlameSaber();
    void OnStartAttack() override;
    void OnEndAttack() override;
    void BeginPlay() override;
    void SuperAttack() override;
    UAudioComponent* PlaySoundFX(USoundCue* cue);
    void Tick(float DeltaTime) override;
    void OnHit(AActor* selfActor, AActor* otherActor,
               FVector NoemalImpulse, const FHitResult& Hit);
    UFUNCTION()
    void OnWeaponOverlapBegin(AActor* other, UPrimitiveComponent* OtherComp,
                              int32 OtherBodyIndex, bool FrontSweep,
                              const FHitResult& Result);
    
    UFUNCTION()
    void OnWeaponOverlapEnd(AActor* other, UPrimitiveComponent* OtherComp,
                            int32 OtherBodyIndex);
	UPROPERTY(EditAnywhere, Category = Weapon)
		TSubclassOf<class AEnemy> EnemyClass;
protected:
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Weapon)
		USkeletalMeshComponent* WeaponMesh;
private:
    //All Sound Effects n Stuff
    UPROPERTY(Transient)
    UAudioComponent* AttackAC;
    UPROPERTY(EditDefaultsOnly, Category= Sound)
    USoundCue* AttackStartSoundFX;
    UPROPERTY(EditDefaultsOnly, Category= Sound)
    USoundCue* AttackEndSoundFX;
    UPROPERTY(EditAnywhere)
    UBoxComponent* BoxCollComp;

	UPROPERTY(EditAnywhere)
	float super_damage = 1.0f;
    bool bIsSuperActive;
    float SuperSphereRadius; //the coll sphere of the super attack
    float SuperAttackTimer; 
	bool first = true;
	USphereComponent* sphere;

	
private:
    //All ParticleEffects and Stuff
	UPROPERTY(EditDefaultsOnly)
	UParticleSystem* sparks;
	UPROPERTY(EditDefaultsOnly)
	UParticleSystem* superball;
    
private:
    //All sound FX stuff here
    UAudioComponent* DamageAC;
    UPROPERTY(EditDefaultsOnly, Category = Sound)
    TArray<USoundCue*> EnemyDamageSFXArray;
    UPROPERTY(EditDefaultsOnly, Category = Sound)
    USoundCue* SuperAttackSFX;
	
    
    
    
    
    
    
};
