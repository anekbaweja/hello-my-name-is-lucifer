// Fill out your copyright notice in the Description page of Project Settings.

#include "TheRealInfinityBlade.h"
#include "PlayerWeapons.h"


// Sets default values
APlayerWeapons::APlayerWeapons()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	
	PrimaryActorTick.bCanEverTick = true;
    
}

APlayerWeapons::~APlayerWeapons()
{
    
}


// Called when the game starts or when spawned
void APlayerWeapons::BeginPlay()
{
    Super::BeginPlay();
    
}


// Called every frame
void APlayerWeapons::Tick( float DeltaTime )
{
    Super::Tick( DeltaTime );
    
}

void APlayerWeapons::OnStartAttack()
{
    
}
void APlayerWeapons::OnEndAttack()
{
    
}

void APlayerWeapons::SuperAttack()
{
    
}
