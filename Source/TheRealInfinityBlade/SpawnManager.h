// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "SpawnManager.generated.h"

UCLASS()
class THEREALINFINITYBLADE_API ASpawnManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawnManager();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	void OnSpawnTimer();
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		bool bossSpawned = false;

private:
	UPROPERTY(EditAnywhere)
	TArray<class ATargetPoint*> SpawnPoints;

	UPROPERTY(EditAnywhere)
	TSubclassOf<ACharacter> CharacterClass;

	UPROPERTY(EditAnywhere)
	float SpawnTime =35.0f;

	UPROPERTY(EditAnywhere)
	int32 type;

	

	int spawnNumber = 1;
	
	int wave = 1;
	//useless comment
};
