// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "PlayerWeapons.generated.h"

UCLASS()
class THEREALINFINITYBLADE_API APlayerWeapons : public AActor
{
    GENERATED_BODY()
    
public:
    // Sets default values for this actor's properties
    APlayerWeapons();
    virtual ~APlayerWeapons();
    virtual void OnStartAttack();
    virtual void OnEndAttack();
    
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;
    
    // Called every frame
    virtual void Tick( float DeltaSeconds ) override;
    virtual void SetWeaponOwner(AActor* owner) {WeaponOwner = owner;}
    virtual AActor* GetWeaponOwner(){return WeaponOwner;}
    virtual void SuperAttack(); 
    bool GetIsWeaponActive() {return bIsWeaponActive;}
protected:
    
    
    UPROPERTY(EditAnywhere, Category = Damage)
    float WeaponDamage;
    
    AActor* WeaponOwner;
    bool bIsWeaponActive; //vhecks if weapon can do damage
    
};
